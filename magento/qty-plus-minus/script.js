
jQuery(function($){ // DOM ready



  /**
   * @author github.com/alex-parra
   */
  if( $('body').hasClass('catalog-product-view') ) { // product page

    /* -------------------------------------------------- */
    /* !- Product Page Qty Plus Minus. Styles in css/custom.css. by AlexParra */
    var productQty = $('.product-view .product-shop .qty-holder');
    if( productQty.length ) {
      
      // Uncomment line below to move qty to same row as price
      // $('.product-view .product-shop .price-box').append(productQty);
      
      var qtyWrap = productQty.find('.qty-wrap');
      var $qty = qtyWrap.find('#qty');
      qtyWrap.on('click', '.qty-btn', function(ev){
        var btn = $(this);
        var qty = parseInt($qty.val());
        if( btn.hasClass('qty-minus') && qty > 1 ) {
          $qty.val(qty-1);
        }
        if( btn.hasClass('qty-plus') ) {
          $qty.val(qty+1);
        }
      });
      qtyWrap.append('<span class="qty-btn qty-plus"></span><span class="qty-btn qty-minus"></span>');
      qtyWrap.addClass('plus-minus');
    }
    /* -------------------------------------------------- */


  } // if body hasClass catalog-product-view




}); // DOM ready