<?php

// https://www.hackerrank.com/challenges/cut-the-sticks

$handle = fopen ("php://stdin","r");
fscanf($handle,"%d",$n);
$arr_temp = fgets($handle);
$arr = explode(" ",$arr_temp);
array_walk($arr,'intval');

do {
  print($n."\n");
  $smallest = min($arr);
  array_walk($arr, function(&$stickLength, $k, $reduce){
    $stickLength -= $reduce;
  }, $smallest);
  $arr = array_filter($arr, function($v){ return ($v > 0); });
  $n = count($arr);
} while( $n > 0 );

