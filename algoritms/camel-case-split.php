<?php

// https://www.hackerrank.com/challenges/camelcase

$handle = fopen ("php://stdin","r");
fscanf($handle,"%s",$s);

// splits saveTheDate to array('save', 'The', 'Date')
$arr = preg_split('/(?=[A-Z])/',$s);

print(count($arr));

