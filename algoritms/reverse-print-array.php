<?php

// https://www.hackerrank.com/challenges/30-arrays

$handle = fopen ("php://stdin","r");
fscanf($handle,"%d",$n);
$arr_temp = fgets($handle);
$arr = explode(" ",$arr_temp);
array_walk($arr,'intval');

for($i = $n-1; $i >= 0; $i--) {
  print($arr[$i]." ");
}

