<?php

// https://www.hackerrank.com/challenges/30-2d-arrays

$handle = fopen ("php://stdin","r");
$arr = array();
for($arr_i = 0; $arr_i < 6; $arr_i++) {
   $arr_temp = fgets($handle);
   $arr[] = explode(" ",$arr_temp);
  array_walk($arr[$arr_i],'intval');
}

$matrixSize = count($arr);
$hourGlassSize = 3;
$maxIndex = $matrixSize - $hourGlassSize;
$minVal = -9;
$highSum = ($minVal*$hourGlassSize) + $minVal + ($minVal*$hourGlassSize);

for( $y=0; $y<=$maxIndex; $y++ ) {
  // $y represents each array line (vertically)
  for( $x=0; $x<=$maxIndex; $x++ ) {
    // $x represents each array column (horizontally)
    $hgSum = 0;
    $hgSum+= $arr[$y][$x] + $arr[$y][$x+1] + $arr[$y][$x+2]; // line 1
    $hgSum+= $arr[$y+1][$x+1]; // line 2
    $hgSum+= $arr[$y+2][$x] + $arr[$y+2][$x+1] + $arr[$y+2][$x+2]; // line 3
    if( $hgSum > $highSum ) {
      $highSum = $hgSum;
    }
  }
}

print($highSum);

