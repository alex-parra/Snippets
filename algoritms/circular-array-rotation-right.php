<?php

// https://www.hackerrank.com/challenges/circular-array-rotation


$_fp = fopen("php://stdin", "r");

list($arraySize, $rotations, $queries) = explode(" ", fgets($_fp));

$intsArray = explode(" ", trim(fgets($_fp)));

$rotations = $rotations % $arraySize; // prevent unneeded rotations

for( $i = 0; $i < $queries; $i++ ) {
  $m = intval(fgets($_fp)) - $rotations;
  if( $m >= 0 ) print($intsArray[$m]."\n");
  else print($intsArray[$m + $arraySize]."\n");
}

