<?php

// https://www.hackerrank.com/challenges/kangaroo

$handle = fopen ("php://stdin","r");
fscanf($handle,"%d %d %d %d",$x1,$v1,$x2,$v2);

$dx = $x1-$x2;
$dv = $v2-$v1;

if( $v1 > $v2 && $dx % $dv == 0 ) {
  print('YES');
} else {
  print('NO');
}

