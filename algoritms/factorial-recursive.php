<?php
$_fp = fopen("php://stdin", "r");
/* Enter your code here. Read input from STDIN. Print output to STDOUT */

function factorial($n) {
  if( $n <= 1 ) return 1;
  return $n * factorial($n-1);
}

$t = intval(fgets($_fp));

print(factorial($t));

