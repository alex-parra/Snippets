<?php
  
// https://www.hackerrank.com/challenges/30-dictionaries-and-maps

$_fp = fopen("php://stdin", "r");
/* Enter your code here. Read input from STDIN. Print output to STDOUT */

$n = fgets($_fp);

$phoneBook = array();

for( $i=0; $i<$n; $i++ ) {
  list($name, $phone) = fscanf($_fp, "%s %s\n");
  $phoneBook[trim($name)] = trim($phone);
}

while( $q = trim(fgets($_fp)) ) {
  if( array_key_exists($q, $phoneBook) ) {
    print($q.'='.$phoneBook[$q]);
  } else {
    print('Not found');
  }
  print("\n");
}

